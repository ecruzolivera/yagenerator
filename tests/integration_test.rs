#[cfg(test)]
mod tests {
    use std::error::Error;
    use std::fs;
    use std::path::Path;
    use yagenerator;
    #[test]
    fn tests_create_files() -> Result<(), Box<dyn Error>> {
        preparate_env()?;
        yagenerator::run("./tests/data.yaml", "./tests/template_config.yaml")?;
        let generated = fs::read_to_string("./tests/generated/IODevice.hpp")?;
        let expected = fs::read_to_string("./tests/expected/IODevice.hpp")?;
        assert_eq!(generated, expected);
        let generated = fs::read_to_string("./tests/generated/TimeDate.h")?;
        let expected = fs::read_to_string("./tests/expected/TimeDate.h")?;
        assert_eq!(generated, expected);
        let generated = fs::read_to_string("./tests/generated/TimeDate.cpp")?;
        let expected = fs::read_to_string("./tests/expected/TimeDate.cpp")?;
        assert_eq!(generated, expected);
        Ok(())
    }

    #[test]
    #[ignore]
    fn tests_generate_cmake_files() -> Result<(), Box<dyn Error>> {
        preparate_env()?;
        yagenerator::run("./tests/data.yaml", "./tests/template_config.yaml")?;
        const GENERATED_FILE_PATH: &str = "./tests/generated/CMakeLists.txt";
        const EXPECTED_FILE_PATH: &str = "./tests/expected/CMakeLists.txt";
        let generated = fs::read_to_string(GENERATED_FILE_PATH)?;
        let expected = fs::read_to_string(EXPECTED_FILE_PATH)?;
        assert_eq!(generated, expected);
        Ok(())
    }

    fn preparate_env() -> Result<(), Box<dyn Error>> {
        const TESTS_GENERATED_FILES_DIR: &str = "./tests/generated";
        if Path::new(TESTS_GENERATED_FILES_DIR).is_dir() {
            fs::remove_dir_all(TESTS_GENERATED_FILES_DIR)?;
            fs::create_dir_all(TESTS_GENERATED_FILES_DIR)?;
        } else {
            fs::create_dir_all(TESTS_GENERATED_FILES_DIR)?;
        }
        fs::copy(
            "./tests/templates/CMakeLists.txt",
            "./tests/generated/CMakeLists.txt",
        )?;
        Ok(())
    }
}
