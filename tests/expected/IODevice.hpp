#ifndef IIODEVICE_HPP
#define IIODEVICE_HPP

namespace IO
{
    class IODevice
    {
    public:
        
        /**
        * @brief IOMode 
        * 
        */
        enum class IOMode {
          
            Read,
          
            Write,
          
            ReadWrite,
        
        };
        

        
        /**
        * @brief Set the read object
        * 
        * @param value 
        */
        virtual void read(int size) = 0;
        
        /**
        * @brief Set the write object
        * 
        * @param value 
        */
        virtual void write(const std::vector<std::uint8_t> &buff) = 0;
        
        /**
        * @brief Set the setIOMode object
        * 
        * @param value 
        */
        virtual void setIOMode(IOMode mode) = 0;
        
        /**
        * @brief Set the getIOMode object
        * 
        * @param value 
        */
        virtual void getIOMode() = 0;
        

    };
} //namespace IO

#endif // IIODEVICE_HPP
