#ifndef TIMEDATE_H
#define TIMEDATE_H

namespace Extra
{
    class TimeDate
    {
    public:
        
        /**
        * @brief Day 
        * 
        */
        enum class Day {
          
            Monday,
          
            Tuesday,
          
            Wednesday,
          
            Thursday,
          
            Friday,
          
            Saturday,
          
            Sunday,
        
        };
        
        /**
        * @brief Format 
        * 
        */
        enum class Format {
          
            ISO,
          
            Metric,
          
            Imperial,
        
        };
        

        
        /**
        * @brief Get the time object
        * 
        * @param value 
        */
        int gettime();
        
        /**
        * @brief Get the date object
        * 
        * @param value 
        */
        int getdate();
        
        /**
        * @brief Get the day object
        * 
        * @param value 
        */
        Day getday();
        
        /**
        * @brief Get the format object
        * 
        * @param value 
        */
        Format getformat();
        

    };
} //namespace Extra

#endif // TIMEDATE_H
