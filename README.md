# Yet another generator

Rust application, if you have a set of structured data and need to generated a bunch of arbitrary types of files from it, this tool can help you to save some time. Is a text generation tool, not a code generation one, the difference is subtle but important. It has no knowledge about the semantics of whatever is generating, for the application everything is text.

Is recommended to first read the [tiny template documentation](https://docs.rs/tinytemplate/1.1.0/tinytemplate/syntax/index.html) in order to understand what that brights to the table.

Can be installed directly with cargo:

```bash
cargo install yagenerator
```

## Examples

See the [examples](./examples):

- [C++ and CMake Code generation](./examples/cpp_cmake_files/cpp_cmake_example.md)

## Details

Generate files given:

- A data [file](./tests/data.yaml)
- A template configuration [file](./tests/templates_config.yaml)
- A set of [templates](./tests/templates/)

Example of use:

```bash
yagenerator -d data.yaml -c template_config.yaml
```

It works by passing the data in the data file "as it is" and with the same structure to the template(s). The template syntax must conform with the requirements of the [TinyTemplate rust crate](https://docs.rs/tinytemplate/1.1.0/tinytemplate/syntax/index.html).

It uses the [unescaped formatter](https://docs.rs/tinytemplate/1.1.0/tinytemplate/fn.format_unescaped.html) and in addition implements the following custom value formatters:

- to_camel_case
- to_class_case
- to_kebab_case
- to_screaming_snake_case
- to_sentence_case
- to_snake_case
- to_title_case
- to_train_case
- to_uppercase

The documentation of the formatters can be found in the [Inflector crate](https://docs.rs/Inflector/0.11.4/inflector/cases/index.html)

## Data File

The data file can be in json or yaml format, is a hashmap (a.k.a dictionary) were each key must be a string, and will serve as the data ID. The values can be arbitrary json or yaml data, and will be passed as is to the templates.

## Template Configuration file

Is a list/array of objects with a well defined [schema](#detailed-schema)

Required field per configuration :

- data_to_use_id: The dictionary key to be used in the data file.
- file_path: The template file to be used.
- destination_path: Where to generate the template.
- operation:
  - Create: Creates the file from scratch using the template, it will overwrite an existing file.
  - Append: Append the generated template to the end of the file, it will thrown an error and stop processing if the file doesn't exist.
  - Insert: Insert the generated template above the specified tag. The tag can be any arbitrary text string, and can have more than one tag on the same file.
- enabled: *true* if you want to generate include the configuration in the file generation, *false* if not.

### Simplified Schema

```yaml
$id: http://example.com/example.json
$schema: http://json-schema.org/draft-07/schema
title: The root schema
type: array
additionalItems: true
default: []
items:
    $id: '#/items'
    anyOf:
    -   $id: '#/items/anyOf/0'
        additionalProperties: true
        title: The first anyOf schema
        type: object
        properties:
            data_to_use_id:
                $id: '#/items/anyOf/0/properties/data_to_use_id'
                default: ''
                title: The data_to_use_id schema
                type: string
            destination_path:
                $id: '#/items/anyOf/0/properties/destination_path'
                default: ''
                title: The destination_path schema
                type: string
            enabled:
                $id: '#/items/anyOf/0/properties/enabled'
                default: false
                title: The enabled schema
                type: boolean
            file_path:
                $id: '#/items/anyOf/0/properties/file_path'
                default: ''
                title: The file_path schema
                type: string
            operation:
                $id: '#/items/anyOf/0/properties/operation'
                default: ''
                title: The operation schema
                type: string
        required:
        - data_to_use_id
        - file_path
        - destination_path
        - operation
        - enabled
```

### Detailed Schema

```yaml
$id: http://example.com/example.json
$schema: http://json-schema.org/draft-07/schema
additionalItems: true
default: []
description: The root schema comprises the entire JSON document.
examples:
-   -   data_to_use_id: IODevice
        destination_path: /tmp/IODevice.hpp
        enabled: true
        file_path: ./tests/templates/Interface.hpp.txt
        operation: Create
    -   data_to_use_id: TimeDate
        destination_path: /tmp/TimeDate.h
        enabled: true
        file_path: ./tests/templates/Class.h.txt
        operation: Create
items:
    $id: '#/items'
    anyOf:
    -   $id: '#/items/anyOf/0'
        additionalProperties: true
        default: {}
        description: An explanation about the purpose of this instance.
        examples:
        -   data_to_use_id: IODevice
            destination_path: /tmp/IODevice.hpp
            enabled: true
            file_path: ./tests/templates/Interface.hpp.txt
            operation: Create
        properties:
            data_to_use_id:
                $id: '#/items/anyOf/0/properties/data_to_use_id'
                default: ''
                description: An explanation about the purpose of this instance.
                examples:
                - IODevice
                title: The data_to_use_id schema
                type: string
            destination_path:
                $id: '#/items/anyOf/0/properties/destination_path'
                default: ''
                description: An explanation about the purpose of this instance.
                examples:
                - /tmp/IODevice.hpp
                title: The destination_path schema
                type: string
            enabled:
                $id: '#/items/anyOf/0/properties/enabled'
                default: false
                description: An explanation about the purpose of this instance.
                examples:
                - true
                title: The enabled schema
                type: boolean
            file_path:
                $id: '#/items/anyOf/0/properties/file_path'
                default: ''
                description: An explanation about the purpose of this instance.
                examples:
                - ./tests/templates/Interface.hpp.txt
                title: The file_path schema
                type: string
            operation:
                $id: '#/items/anyOf/0/properties/operation'
                default: ''
                description: An explanation about the purpose of this instance.
                examples:
                - Create
                title: The operation schema
                type: string
        required:
        - data_to_use_id
        - file_path
        - destination_path
        - operation
        - enabled
        title: The first anyOf schema
        type: object
title: The root schema
type: array

```
