use std::process;

use clap::{crate_authors, crate_version, App, AppSettings, Arg};

fn main() {
    let app = App::new("yagenerator")
        .version(crate_version!())
        .author(crate_authors!())
        .about("Yet Another Generator, can create new files, append or insert arbitrary text to to existing files,
                given a set of templates, a data and a configuration files")
        .setting(AppSettings::ArgRequiredElseHelp)
        .arg(
            Arg::with_name("data_file")
                .short("d")
                .value_name("DATA_FILE")
                .required(true)
                .takes_value(true)
                .help("Data file in YAML format https://en.wikipedia.org/wiki/YAML"),
        )
        .arg(
            Arg::with_name("template_config_file")
                .short("c")
                .value_name("TEMPLATE_CONFIG_FILE")
                .required(true)
                .takes_value(true)
                .help(
                    "Template configuration file in YAML format https://en.wikipedia.org/wiki/YAML",
                ),
        );

    let matches = app.get_matches();
    let data_file_path = matches
        .value_of("data_file")
        .expect("No configuration file was provided");
    let template_config_file_path = matches
        .value_of("template_config_file")
        .expect("No configuration file was provided");

    if let Err(e) = yagenerator::run(data_file_path, template_config_file_path) {
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}
