use inflector::Inflector;
use serde_json;
use std::fmt::Write;
use tinytemplate::{error::Error as TinyError, error::Result as TinyResult};

macro_rules! impl_formatter {
    ($formatter:ident) => {
        pub fn $formatter(value: &serde_json::Value, output: &mut String) -> TinyResult<()> {
            match value {
                serde_json::Value::Null => Ok(()),
                serde_json::Value::Bool(b) => {
                    write!(output, "{}", b)?;
                    Ok(())
                }
                serde_json::Value::Number(n) => {
                    write!(output, "{}", n)?;
                    Ok(())
                }
                serde_json::Value::String(s) => {
                    output.push_str(&s.$formatter());
                    Ok(())
                }
                _ => Err(TinyError::GenericError {
                    msg: "Expected a printable value but found array or object.".to_string(),
                }),
            }
        }
    };
}

impl_formatter!(to_camel_case);
impl_formatter!(to_class_case);
impl_formatter!(to_kebab_case);
impl_formatter!(to_screaming_snake_case);
impl_formatter!(to_sentence_case);
impl_formatter!(to_snake_case);
impl_formatter!(to_title_case);
impl_formatter!(to_train_case);
impl_formatter!(to_uppercase);
