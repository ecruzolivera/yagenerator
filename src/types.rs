use serde::Deserialize;
use serde_yaml;
use std::collections::HashMap;
use std::path::PathBuf;

#[derive(Deserialize, Debug, PartialEq)]
pub struct Template {
    pub data_to_use_id: String,
    pub file_path: PathBuf,
    pub destination_path: PathBuf,
    pub operation: Operation,
    pub enabled: bool,
}
#[derive(Deserialize, Debug, PartialEq)]
pub enum Operation {
    Append,
    Create,
    Insert(String),
}

pub struct TextFile {
    pub file: PathBuf,
    pub content: String,
}

pub type Data = HashMap<String, serde_yaml::Value>;
