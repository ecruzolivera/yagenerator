use regex::Regex;
use std::error::Error;
use std::fs;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::Write;
use tinytemplate::TinyTemplate;

mod types;
use crate::types::*;

mod formatters;
use crate::formatters::*;

pub fn run(data_file_path: &str, template_file_path: &str) -> Result<(), Box<dyn Error>> {
    let data = parse_data_file(data_file_path)?;
    let template_config = parse_template_config_file(template_file_path)?;

    let templates_content = template_config
        .iter()
        .map(|tc| (tc.file_path.file_name(), fs::read_to_string(&tc.file_path)))
        .collect::<Vec<_>>();

    let mut t_engine = TinyTemplate::new();
    t_engine.set_default_formatter(&tinytemplate::format_unescaped);

    t_engine.add_formatter("to_camel_case", to_camel_case);
    t_engine.add_formatter("to_class_case", to_class_case);
    t_engine.add_formatter("to_kebab_case", to_kebab_case);
    t_engine.add_formatter("to_screaming_snake_case", to_screaming_snake_case);
    t_engine.add_formatter("to_sentence_case", to_sentence_case);
    t_engine.add_formatter("to_snake_case", to_snake_case);
    t_engine.add_formatter("to_title_case", to_title_case);
    t_engine.add_formatter("to_train_case", to_train_case);
    t_engine.add_formatter("to_uppercase", to_uppercase);

    for tc in &templates_content {
        match tc {
            (Some(id), Ok(content)) => {
                match t_engine.add_template(id.to_str().unwrap_or_default(), &content) {
                    Err(e) => eprintln!(
                        "Error parsing template {}: {}",
                        id.to_str().unwrap_or_default(),
                        e.to_string()
                    ),
                    Ok(()) => (),
                }
            }
            (Some(id), Err(e)) => eprintln!(
                "Error reading template {}: {}",
                id.to_str().unwrap_or_default(),
                e.to_string()
            ),
            (None, _) => eprintln!("Unknown Error reading template"),
        }
    }

    for template in &template_config {
        if let Some(data_value) = data.get(&template.data_to_use_id) {
            let rendered = t_engine.render(
                &template
                    .file_path
                    .file_name()
                    .unwrap_or_default()
                    .to_str()
                    .unwrap_or_default(),
                data_value,
            )?;
            let file = TextFile {
                file: template.destination_path.clone(),
                content: rendered,
            };
            match &template.operation {
                Operation::Append => append_to_file(file)?,
                Operation::Create => create_file(file)?,
                Operation::Insert(token) => insert_in_file(file, token)?,
            }
        } else {
            eprintln!("data_to_use_id={} not found", template.data_to_use_id);
        }
    }
    Ok(())
}

fn parse_data_file(file: &str) -> Result<Data, Box<dyn Error>> {
    let f = File::open(file)?;
    let d = serde_yaml::from_reader(f)?;
    Ok(d)
}

fn parse_template_config_file(file: &str) -> Result<Vec<Template>, Box<dyn Error>> {
    let f = File::open(file)?;
    let d: Vec<Template> = serde_yaml::from_reader(f)?;
    Ok(d)
}

fn append_to_file(file_text: TextFile) -> Result<(), Box<dyn Error>> {
    let mut file = OpenOptions::new().append(true).open(file_text.file)?;
    file.write_all(file_text.content.as_bytes())?;
    Ok(())
}

fn create_file(file_text: TextFile) -> Result<(), Box<dyn Error>> {
    let mut file = OpenOptions::new()
        .create(true)
        .write(true)
        .open(file_text.file)?;
    file.write_all(file_text.content.as_bytes())?;
    Ok(())
}

fn insert_in_file(file_text: TextFile, token: &str) -> Result<(), Box<dyn Error>> {
    let re = Regex::new(token)?;
    let file_as_text = fs::read_to_string(&file_text.file)?;
    let mut text_lines = file_as_text.split('\n').collect::<Vec<_>>();
    if let Some(index) = text_lines.iter().position(|v| re.is_match(*v)) {
        text_lines.insert(index, &file_text.content);
        let file_content = text_lines.join("\n");
        fs::write(&file_text.file, file_content.as_bytes())?;
        Ok(())
    } else {
        Err(format!(
            "Token {} not found on file: {}",
            token,
            file_text.file.to_str().unwrap_or("unknown file")
        ))?
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_yaml::Value;
    use std::collections::HashMap;
    use std::path::PathBuf;
    #[test]
    fn tests_parse_data_file() -> Result<(), Box<dyn Error>> {
        let mut expected: HashMap<String, serde_yaml::Value> = HashMap::new();
        let value = serde_yaml::from_str::<Value>(
            r##"
            {
                "name": "TimeDate",
                "namespace": "Extra",
                "methods": [
                  {
                    "name": "time",
                    "ret_type": "int"
                  },
                  {
                    "name": "date",
                    "ret_type": "int"
                  },
                  {
                    "name": "day",
                    "ret_type": "Day"
                  },
                  {
                    "name": "format",
                    "ret_type": "Format"
                  }
                ],
                "enumerations": [
                  {
                    "name": "Day",
                    "variants": [
                      "Monday",
                      "Tuesday",
                      "Wednesday",
                      "Thursday",
                      "Friday",
                      "Saturday",
                      "Sunday"
                    ]
                  },
                  {
                    "name": "Format",
                    "variants": [
                      "ISO",
                      "Metric",
                      "Imperial"
                    ]
                  }
                ]
              }
            "##,
        )?;
        expected.insert(String::from("TimeDate"), value);
        let value = serde_yaml::from_str::<Value>(
            r##"
            {
                "name": "IODevice",
                "namespace": "IO",
                "methods": [
                  {
                    "identifier": "read",
                    "arg_type": "int size",
                    "ret_type": "std::vector<std::uint8_t>"
                  },
                  {
                    "identifier": "write",
                    "arg_type": "const std::vector<std::uint8_t> &buff",
                    "ret_type": "int"
                  },
                  {
                    "identifier": "setIOMode",
                    "arg_type": "IOMode mode",
                    "ret_type": "bool"
                  },
                  {
                    "identifier": "getIOMode",
                    "arg_type": null,
                    "ret_type": "IOMode"
                  }
                ],
                "enumerations": [
                  {
                    "name": "IOMode",
                    "variants": [
                      "Read",
                      "Write",
                      "ReadWrite"
                    ]
                  }
                ]
              }
            "##,
        )?;
        expected.insert(String::from("IODevice"), value);
        let value = serde_yaml::from_str::<Value>(
            r##"
            {
                "files": [TimeDate.cpp, TimeDate.h, IODevice.hpp]
            }
            "##,
        )?;
        expected.insert(String::from("SourceFiles"), value);
        const CONFIG_PATH: &str = "tests/data.yaml";
        let parsed = parse_data_file(CONFIG_PATH)?;
        assert_eq!(parsed.len(), 3);
        assert_eq!(expected["TimeDate"], parsed["TimeDate"]);
        assert_eq!(expected["IODevice"], parsed["IODevice"]);
        assert_eq!(expected["SourceFiles"], parsed["SourceFiles"]);
        Ok(())
    }

    #[test]
    fn tests_parse_template_config_file() -> Result<(), Box<dyn Error>> {
        let expected = vec![
            Template {
                data_to_use_id: String::from("IODevice"),
                file_path: PathBuf::from("./tests/templates/Interface.hpp.txt"),
                destination_path: PathBuf::from("./tests/generated/IODevice.hpp"),
                operation: Operation::Create,
                enabled: true,
            },
            Template {
                data_to_use_id: String::from("TimeDate"),
                file_path: PathBuf::from("./tests/templates/Class.h.txt"),
                destination_path: PathBuf::from("./tests/generated/TimeDate.h"),
                operation: Operation::Create,
                enabled: true,
            },
            Template {
                data_to_use_id: String::from("TimeDate"),
                file_path: PathBuf::from("./tests/templates/Class.cpp.txt"),
                destination_path: PathBuf::from("./tests/generated/TimeDate.cpp"),
                operation: Operation::Create,
                enabled: true,
            },
            Template {
                data_to_use_id: String::from("SourceFiles"),
                file_path: PathBuf::from("./tests/templates/CMakeSnippet.txt"),
                destination_path: PathBuf::from("./tests/generated/CMakeLists.txt"),
                operation: Operation::Insert(String::from("# YAYGENERATOR INSERT TAG")),
                enabled: true,
            },
        ];
        const CONFIG_PATH: &str = "./tests/template_config.yaml";
        let parsed = parse_template_config_file(CONFIG_PATH)?;
        assert_eq!(expected[0], parsed[0]);
        assert_eq!(expected[1], parsed[1]);
        assert_eq!(expected[2], parsed[2]);
        assert_eq!(expected[3], parsed[3]);
        Ok(())
    }
    
}
